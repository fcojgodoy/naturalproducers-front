# naturalProducerFront

> My praiseworthy Nuxt.js project

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## To Do

Remember always view last commits.

## Development

### Regenerate favicon and app icons using RealFaviconGenerator

Install globally cli-real-favicon:

`npm install -g cli-real-favicon`

Edit `faviconDescription.json`.

The configuration can add or remove links or meta tags, which would require editing the `seo/favicon-meta.js` configuration.

Generate the icons:

```bash
mkdir outputDir
real-favicon generate faviconDescription.json faviconData.json static
```

More info in https://realfavicongenerator.net/

## Project

### Posible features

- Keyboardfriendly ;)

### Features

#### User and producers

- El usuario puede distinguir sus productores
- El usuario puede distinguir sus productores drafts
- El usuario puede ver solo sus productores
