import faviconTags from './favicon-tags'

export default {
  titleTemplate: '%s - ' + 'Natural Producers',
  title: 'Natural Producers',
  htmlAttrs: {
    lang: 'es'
  },
  meta: [
    { charset: 'utf-8' },
    { name: 'viewport', content: 'width=device-width, initial-scale=1' },
    {
      hid: 'description',
      name: 'description',
      content:
        'Descubre a productores y distribuidores que respetan la naturaleza y las personas.'
    },
    {
      hid: 'og:title',
      property: 'og:title',
      content: 'Natural Producers'
    },
    {
      hid: 'og:description',
      property: 'og:description',
      content:
        'Descubre a productores y distribuidores que respetan la naturaleza y las personas.'
    },
    {
      hid: 'og:type',
      property: 'og:type',
      content: 'website'
    },
    {
      hid: 'og:image',
      property: 'og:image',
      content: '/favicon.png'
    },
    {
      hid: 'og:url',
      property: 'og:url',
      content: 'https://naturalproducers.org'
    },
    {
      property: 'og:locale',
      content: 'es_ES'
    },
    ...faviconTags.faviconMeta
  ],
  link: [
    { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' },
    {
      hid: 'canonical',
      rel: 'canonical',
      href: 'https://naturalproducers.org'
    },
    ...faviconTags.faviconLinks
  ],
  script: [
    {
      hid: 'goatcounter', 
      'data-goatcounter-settings': '{ "allow_local": true }',
      'data-goatcounter': process.env.GOATCOUNTER_URL,
      async: true,
      src: '//gc.zgo.at/count.js'
    }
  ]
}
