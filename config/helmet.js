export default {
  crossOriginEmbedderPolicy: false,
  hsts: {
    maxAge: 31536000, // (≈ 1 year)
    preload: true
  }
}
