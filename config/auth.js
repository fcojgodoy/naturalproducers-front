export default {
  redirect: {
    login: '/users/signin',
    home: '/'
  },
  strategies: {
    local: {
      scheme: 'refresh',
      refreshToken: {
        property: 'refreshToken',
        data: 'refreshToken',
        maxAge: 60 * 60 * 24 * 10,
        tokenRequired: true
      },
      token: {
        property: 'token'
      },
      user: {
        property: false
      },
      endpoints: {
        login: { url: '/users/signin', method: 'post' },
        logout: { url: '/users/signout', method: 'post' },
        refresh: { url: '/users/token', method: 'post' },
        user: { url: '/users/me', method: 'get' }
      }
    }
  }
}
