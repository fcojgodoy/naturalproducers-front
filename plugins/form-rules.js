export default (_context, inject) => {
  // Inject $hello(msg) in Vue, context and store.
  inject('rules', () => ({
    counter: (value) =>
      (value && value.length <= 100) || 'No más de 100 carácteres.',
    email: (value) => {
      const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      return (
        value === undefined ||
        value === null ||
        value === '' ||
        pattern.test(value) ||
        'Parece que el email no está bien escrito.'
      )
    },
    number: (value) => typeof value === 'number' || 'Debe ser un número.',
    required: (value) => {
      if (Array.isArray(value)) {
        return (!!value && Boolean(value.length)) || 'Se debe elegir al menos una opción.'
      } else {
        return !!value || 'Este campo es necesario.'
      }
    }
  }))
}
