import qs from 'qs';

export default function ({ $axios, store, redirect }) {
  $axios.onError((error) => {
    const code = parseInt(error.response && error.response.status)
    if (code === 503) {
      redirect('/maintenance')
    }
    if (code === 403) {
      redirect('/404')
    }
    if (code === 401) {
      store.commit('snackbar/SHOW_SNACKBAR', { text: 'Sesión caducada. Vuelve a inciarla' })
    }
  })

  // Format nested params correctly
  $axios.interceptors.request.use((config) => {
    config.paramsSerializer = (params) => qs.stringify(params)
    return config
  })
}
