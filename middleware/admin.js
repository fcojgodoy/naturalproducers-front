export default ({ $auth, redirect }) => {
  if (!$auth.loggedIn || $auth.user.role !== 'admin') {
    return redirect('/producers')
  }
}
