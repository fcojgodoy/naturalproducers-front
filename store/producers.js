import { make } from 'vuex-pathify'

const state = () => ({
  formValidity: false,
  list: {
    // limit: 2
  },
  listPaginated: {},
  listPaginatedLoading: false,
  listOfFavourites: [],
  listOfMine: [],
  municipalities: [],
  pagination: {
    length: null,
    pageIndex: null,
    pageTotal: null
  },
  producer: {
    activisms: [],
    certifications: [],
    contactEmails: [],
    contactPhones: [],
    contactWebs: [],
    description: null,
    locations: [],
    name: null,
    needsCovered: [],
    organizationalType: null,
    productCategories: [],
    relations: {},
    types: []
  },

  producerRelationToAdd: {
    relationType: undefined,
    producer: undefined
  },

  producerRelationSaving: false,

  producerRelationRemoving: false,

  productChapterEditing: null
})

const getters = {
  geomaped(state) {
    if (!state.producer.locations.length) {
      throw new Error('Producer have not locations!')
    } else {
      return {
        ...state.producer.locations[0],
        producer: { ...state.producer }
      }
    }
  },

  listGeomaped(state) {
    let result = state.list.docs.filter((producer) => producer.locations.length)
    result = result.map((producer) => {
      return (producer = {
        ...producer.locations[0],
        producer: { ...producer }
      })
    })
    return result
  },

  producerWithoutSpecialProps(state) {
    const { relations, ...producer } = state.producer
    return producer
  },

  productCategoriesIds(state) {
    return state.producer.productCategories.map((productCategory) => {
      return productCategory._id
    })
  },

  producerRelationToAddTypeCode: (state) =>
    state.producerRelationToAdd.relationType.code
}

const mutations = {
  ...make.mutations(state),

  CLEAN_PRODUCER(state) {
    state.producer = {
      activisms: [],
      certifications: [],
      contactEmails: [],
      contactPhones: [],
      contactWebs: [],
      description: null,
      locations: [],
      name: null,
      needsCovered: null,
      organizationalType: null,
      productCategories: [],
      relations: {},
      types: []
    }
  },

  CLEAN_PRODUCER_RELATION_TO_ADD(state) {
    state.producerRelationToAdd = {
      relationType: undefined,
      producer: undefined
    }
  },

  REMOVE_MY_PRODUCER: (state, producerId) => {
    const producerToDelete = state.listOfMine
      .map((producer) => producer._id)
      .indexOf(producerId)
    state.listOfMine.splice(producerToDelete, 1)
  },

  REMOVE_PRODUCER_RELATION(state, payload) {
    let producerRelationByType =
      state.producer.relations[payload.producerRelationTypeCode]
    producerRelationByType = producerRelationByType.filter(
      (producer) => producer._id !== payload.producer._id
    )
    if (!producerRelationByType.length) {
      delete state.producer.relations[payload.producerRelationTypeCode]
    }
    state.producer.relations = { ...state.producer.relations }
  }
}

const actions = {
  // TODO: context.getters.producerWithoutSpecialProps
  async createOne(context) {
    const relations = { ...context.state.producer.relations }
    Object.keys(relations).forEach((relationType) => {
      relations[relationType] = relations[relationType].map(
        (producer) => producer._id
      )
    })
    const producer = {
      ...context.state.producer,
      relations,
      status: ['supervisor', 'admin'].includes(this.$auth.state.user.role)
        ? 2
        : 1
    }
    await this.$axios.$post('/producers/create', producer)
  },

  async delete({ commit }, producerId) {
    await this.$axios.$delete(`/producers/${producerId}`)
    commit('REMOVE_MY_PRODUCER', producerId)
  },

  async getAll({ commit, rootState }) {
    const res = await this.$axios.$get('/producers', {
      params: { ...rootState.producersfilter.query, noPagination: 1 }
    })
    commit('SET_LIST', res)
  },

  async getPaginated({ commit, state, rootState }) {
    state.listPaginatedLoading = true
    const res = await this.$axios.$get('/producers', {
      params: {
        ...rootState.producersfilter.query,
        page: state.listPaginated.page,
        limit: state.listPaginated.limit
      }
    })
    commit('SET_LIST_PAGINATED', res)
    state.listPaginatedLoading = false
  },

  async getListOfFavourite({ commit }) {
    const producers = await this.$axios.$get('/producers/user/favourites')
    commit('SET_LIST_OF_FAVOURITES', producers)
  },

  async getListOfMine({ commit }) {
    const producers = await this.$axios.$post('/producers/mine')
    commit('SET_LIST_OF_MINE', producers)
  },

  async getProducer({ commit }, payload) {
    try {
      const producer = await this.$axios.$get(`/producers/${payload}`)
      commit('SET_PRODUCER', producer)
    } catch (error) {
      console.log('Error getting Producer', error)
    }
  },

  addProducerRelation({ state }, _payload) {
    const relationCode = state.producerRelationToAdd.relationType.code
    const producerToAdd = state.producerRelationToAdd.producer
    const add = () => {
      state.producer.relations[relationCode].push(producerToAdd)
      state.producer.relations = { ...state.producer.relations }
    }

    if (!state.producer.relations) {
      state.producer.relations = {}
    }

    if (!state.producer.relations[relationCode]) {
      state.producer.relations[relationCode] = []
      add()
    } else {
      const exists = state.producer.relations[relationCode].findIndex(
        (producer) => producer._id === producerToAdd._id
      )
      if (exists === -1) {
        add()
      }
    }
  },

  async updateProducerAddRelation({ commit, state }, _payload) {
    try {
      state.producerRelationSaving = true
      await this.$axios.$patch(`/producers/${state.producer._id}/addRelation`, {
        relationProducerId: state.producerRelationToAdd.producer._id,
        relationType: state.producerRelationToAdd.relationType.code
      })
      state.producerRelationSaving = false
      commit(
        'snackbar/SHOW_SNACKBAR',
        { text: 'Relación guardada' },
        { root: true }
      )
    } catch (err) {
      console.error(err)
    }
  },

  async updateProducerRemoveRelation({ state, commit }, payload) {
    try {
      commit('REMOVE_PRODUCER_RELATION', payload)
      state.producerRelationRemoving = true
      await this.$axios.$patch(
        `/producers/${state.producer._id}/removeRelation`,
        {
          relationProducerId: payload.producer._id,
          relationType: payload.producerRelationTypeCode
        }
      )
      state.producerRelationRemoving = false
      commit(
        'snackbar/SHOW_SNACKBAR',
        { text: 'Relación eliminada' },
        { root: true }
      )
    } catch (err) {
      console.error(err)
    }
  },

  async updateProducer({ commit, getters }, payload) {
    await this.$axios.$put(
      `/producers/${payload._id}`,
      getters.producerWithoutSpecialProps
    )
    commit(
      'snackbar/SHOW_SNACKBAR',
      { text: 'Productor actualizado' },
      { root: true }
    )
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
