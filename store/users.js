import { make } from 'vuex-pathify'

const state = () => ({
  isLoggingInUser: false,
  signinDialog: false,
  signupDialog: false,
})

const mutations = {
  ...make.mutations(state)
}

const actions = {
  // ...make.actions(state),

  async signin({ commit }, payload) {
    commit('SET_IS_LOGGING_IN_USER', true)
    await this.$auth.loginWith('local', { data: payload })
    commit('SET_IS_LOGGING_IN_USER', false)
  },

  async signup(_context, payload) {
    return await this.$axios.post('/users/signup', payload)
  },

  async updateOne(_context, payload) {
    await this.$axios.$put(`/users/${payload._id}`, payload.form)
  },

  async toggleFavourite(_context, { producerId, add }) {
    const userUpdated = await this.$axios.$patch(
      `/users/${this.$auth.user._id}/producers/favourites/${add ? 'add' : 'remove'
      }`,
      { producerId }
    )
    this.$auth.setUser(userUpdated)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
