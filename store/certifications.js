import { make } from 'vuex-pathify'

const state = () => ({
  list: []
})

const mutations = make.mutations(state)

const actions = {
  async setList({ commit }) {
    const certifications = await this.$axios.$get('/certifications')
    commit('SET_LIST', certifications)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
