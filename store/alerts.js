import { make } from 'vuex-pathify'

const state = () => ({
  betaWarning: {
    visible: true
  }
})

const mutations = {
  ...make.mutations(state)
}

export default {
  namespaced: true,
  state,
  mutations
}
