import { make } from 'vuex-pathify'

const state = () => ({
  list: []
})

const mutations = {
  ...make.mutations(state)
}

const actions = {
  async setList({ commit }) {
    const types = await this.$axios.$get('/producerTypes')
    commit('SET_LIST', types)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
