import { make } from 'vuex-pathify'

const state = () => ({
  list: []
})

const mutations = make.mutations(state)

const actions = {
  async setList({ commit }) {
    const organizationalTypes = await this.$axios.$get('/organizational-types')
    commit('SET_LIST', organizationalTypes)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
