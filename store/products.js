import { make } from 'vuex-pathify'

const state = () => ({
  chapters: [],
  headings: [],
  formValidity: false,
  list: [],
  product: null
})

const getters = {
  productHeadingsByProductChapter: (state) => (chapterId) => {
    return state.list.filter((i) => i.belongTo === chapterId)
  }
}

const mutations = {
  ...make.mutations(state),

  CLEAN_PRODUCT(state) {
    state.product = {
      _id: null,
      alternateName: {},
      belongTo: undefined,
      name: {
        es_es: null,
        en_us: null
      },
      descriptions: {},
      keywords: {},
      metaData: {
        iconCode: null,
        iconColor: ''
      },
      type: null
    }
  }
}

const actions = {
  async fetchList({ commit }) {
    const products = await this.$axios.$get('/products')
    commit('SET_LIST', products)
  },

  async fetchChapters({ commit }) {
    const chapters = await this.$axios.$get('/products/chapters')

    // Mapping DTO to requirements
    const result = []
    chapters.forEach((chapter) => {
      result.push({ chapter, headings: [] })
    })

    commit('SET_CHAPTERS', result)
  },

  async getProduct({ commit }, payload) {
    const product = await this.$axios.$get(`/products/${payload}`)
    // TODO: add alternateName and keywords in all documents and remove this
    const productFull = {
      alternateName: { en_us: null, es_es: null },
      keywords: { en_us: null, es_es: null },
      ...product
    }
    commit('SET_PRODUCT', productFull)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
