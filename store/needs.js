import { make } from 'vuex-pathify'

const state = () => ({
  list: []
})

const mutations = make.mutations(state)

const actions = {
  async getList({ commit }) {
    const needs = await this.$axios.$get('/needs')
    commit('SET_LIST', needs)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
