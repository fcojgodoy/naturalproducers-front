import { make } from 'vuex-pathify'

const state = () => ({
  filters: {
    activisms: [],
    certifications: [],
    needs: [],
    productCategories: [],
    types: []
  },
  pagination: {
    pageIndex: 1,
    pageSize: 2
  },
  query: null,
  sort: {
    location: null
  }
})

const getters = {
  locationOrFiltersIsActive(state) {
    return Boolean(
      state.sort.location ||
        state.filters.activisms.length ||
        state.filters.needs.length ||
        state.filters.productCategories.length ||
        state.filters.types.length
    )
  },

  filterActivismsCopy({ store }) {
    store.copy('filter@activisms')
  },

  sortLocationReversedCoordinates: state =>
    state.sort.location?.geometry.coordinates.reverse()
}

const mutations = {
  ...make.mutations(state),

  REMOVE_PRODUCT_FILTER(state, i) {
    state.filters.productCategories.splice(i, 1)
  },

  REMOVE_PRODUCTS_FILTER(state) {
    state.filters.productCategories.splice(
      0,
      state.filters.productCategories.length
    )
  }
}

const actions = {
  setFilters(context, payload) {
    context.commit('SET_FILTERS', { ...context.state.filters, ...payload })
  },

  /**
   * Building the filter in Mongoose style
   * @see https://mongoosejs.com/docs/api.html#model_Model.find
   *
   */
  createQuery({ state, commit }) {
    const query = {
      geoNear: {},
      match: {}
    }

    // Activisms
    if (state.filters.activisms && state.filters.activisms.length) {
      const activismsIds = state.filters.activisms.map((a) => a._id)
      query.match.activisms = { $in: activismsIds }
    }

    // Product Categories
    if (
      state.filters.productCategories &&
      state.filters.productCategories.length
    ) {
      const productCategoriesIds = state.filters.productCategories.map(
        (productCategory) => productCategory._id
      )
      query.match.productCategories = { $in: productCategoriesIds }
    }

    // Certifications
    if (state.filters.certifications && state.filters.certifications.length) {
      const certificationsIds = []
      state.filters.certifications.forEach((certification) => {
        certificationsIds.push(certification._id)
      })
      query.match.certifications = { $in: certificationsIds }
    }

    // Needs
    if (state.filters.needs && state.filters.needs.length) {
      query.match.needsCovered = { $in: state.filters.needs }
    }

    // Types
    if (state.filters.types && state.filters.types.length) {
      const typesIds = state.filters.types.map((e) => e._id)
      query.match.types = { $in: typesIds }
    }

    // Location
    if (state.sort.location) {
      query.geoNear.location = state.sort.location.geometry
    } else {
      delete query.geoNear
    }

    commit('SET_QUERY', query)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
