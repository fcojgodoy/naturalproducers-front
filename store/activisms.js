import { make } from 'vuex-pathify'

const state = () => ({
  list: []
})

const mutations = make.mutations(state)

const actions = {
  async setList({ commit }) {
    const activisms = await this.$axios.$get('/activisms')
    commit('SET_LIST', activisms)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
