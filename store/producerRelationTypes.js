import { make } from 'vuex-pathify'

const state = () => ({
  list: []
})

const getters = {
  producerRelationTypesNames(state) {
    const result = {}
    state.list.forEach((relation) => {
      result[relation.code] = relation.name.es_es
    })
    return result
  }
}

const mutations = make.mutations(state)

const actions = {
  async setList({ commit }) {
    const producerRelationTypes = await this.$axios.$get(
      '/producer-relation-types'
    )
    commit('SET_LIST', producerRelationTypes)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
