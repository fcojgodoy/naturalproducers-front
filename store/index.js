import { Store } from 'vuex'
import pathify from './pathify'

import alerts from './alerts'
import activisms from './activisms'
import certifications from './certifications'
import locations from './locations'
import needs from './needs'
import organizationalTypes from './organizationalTypes'
import producers from './producers'
import producerRelationTypes from './producerRelationTypes'
import producerTypes from './producerTypes'
import producersfilter from './producersfilter'
import products from './products'
import snackbar from './snackbar'
import users from './users'

const createStore = () => {
  return new Store({
    plugins: [pathify.plugin],

    modules: {
      alerts,
      activisms,
      certifications,
      locations,
      needs,
      organizationalTypes,
      producerRelationTypes,
      producerTypes,
      producers,
      producersfilter,
      products,
      snackbar,
      users
    },

    actions: {
      nuxtServerInit(_context, _value) {
        console.log('NUXT_SERVER_INIT!')
      }
    }
  })
}
export default createStore
