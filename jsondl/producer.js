export default (producer, location) => {
  return {
    '@context': 'https://schema.org',
    '@type': 'Organization',
    // Properties from Organization
    ...(location && {
      address: {
        '@type': 'PostalAddress',
        addressCountry: location.properties.address.country,
        addressLocality:
          location.properties.address.city ||
          location.properties.address.place ||
          location.properties.address.boundary ||
          location.properties.address.town ||
          location.properties.address.hamlet ||
          location.properties.address.village,
        addressRegion:
          location.properties.address.state ||
          location.properties.address.county ||
          location.properties.address.state_district,
        postalCode: location.properties.address.postcode,
        streetAddress:
          location.properties.address.road +
          location.properties.address.house_number,
      },
    }),
    email: producer.contactEmails[0],
    ethicsPolicy: undefined,
    hasCredential: undefined,
    hasOfferCatalog: {
      '@type': 'OfferCatalog',
      name: 'Categorías de productos ofrecidos',
      numberOfItems: producer.productCategories.length,
      itemListElement: producer.productCategories.map(
        (productCategory, index) => ({
          '@type': 'Product',
          position: index + 1,
          item: {
            '@id': productCategory._id,
            name: productCategory.name.es_es,
            keywords:
              productCategory.keywords && productCategory.keywords.es_es,
            alternateName:
              productCategory.alternateName &&
              productCategory.alternateName.es_es,
            description:
              productCategory.description && productCategory.description.es_es,
          },
        })
      ),
    },
    hasPOS: undefined,
    member: undefined,
    memberOf: undefined,
    ownershipFundingInfo:
      producer.organizationalType && producer.organizationalType.name.es_es,
    telephone: producer.contactPhones && producer.contactPhones[0],
    // Properties from Thing
    description: producer.description,
    name: producer.name,
    sameAs: producer.contactWebs && producer.contactWebs[0],
    url: 'https://naturalproducers.org/producers/' + producer._id,
  }
}
